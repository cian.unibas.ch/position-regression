import torch
from torch.utils.data import Dataset
import os
import os.path
import nibabel as nib


class SingleClassDataset(Dataset):
    def __init__(self, dir_path):
        super().__init__()
        self.dir = dir_path
        self.files = os.listdir(self.dir)
        self.files = [x for x in self.files if 'nii.gz' in x]

    def __len__(self):
        return len(self.files)

    def __getitem__(self, idx):
        filename = self.files[idx]
        fp = os.path.join(self.dir, filename)
        nib_img = nib.load(fp)
        img = torch.tensor(nib_img.get_fdata())
        return dict(img=img, path=fp, name=filename)


