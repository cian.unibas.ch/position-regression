import torch
from torch.utils.data import Dataset
import os
import os.path
import nibabel as nib
import pandas as pd
import numpy as np


class SingleClassDataset(Dataset):
    def __init__(self, cfg, part='train'):
        super().__init__()
        self.cfg = cfg
        self.dir = cfg.data.image_path  # base dir
        self.db = pd.read_csv(cfg.data.database)
        files = os.listdir(self.dir)
        # check database files are contained
        if part == 'train':
            filter_mask = self.db['train']
        elif part == 'test':
            filter_mask = self.db['test']
        elif part == 'all':
            filter_mask = self.db['use']
        else:
            raise ValueError(f'you provided an invalid argument {part=}')
        self.db_use = self.db[filter_mask]
        files = [x for x in files if 'nii.gz' in x]
        db_set = set(self.db_use['file'])
        file_set = set(files)
        if not (db_set <= file_set):
            not_found_files = "\n".join(list(file_set - db_set))
            raise FileNotFoundError(fr'db contains files that cannot be found: \n {not_found_files}')
        self.histeq = HistEq()

    def __len__(self):
        return len(self.db_use)

    def __getitem__(self, idx):
        db_row = self.db_use.iloc[idx]
        filename = db_row['file']
        fp_img = os.path.join(self.dir, filename)
        fp_mask = os.path.join(self.dir, '..', 'dataset_masks', filename)
        nib_img = nib.load(fp_img)
        nib_mask = nib.load(fp_mask)
        img = nib_img.get_fdata()
        mask = torch.tensor(nib_mask.get_fdata())
        mask = mask >= 2
        if self.cfg.data.hist_eq:
            self.histeq.read_params(img[mask])
            img = self.histeq.apply(img)
        else:
            img = normalize(img)
        img = torch.tensor(img)
        return dict(img=img, mask=mask, path=fp_img, affine=nib_img.affine, db_idx=idx, name=filename)


def normalize(img):
    img -= -1024
    img /= 4096
    return img


class HistEq:
    def __init__(self):
        self.cdf = None
        self.hist = None
        self.bins = torch.arange(-1024.5, 3072.5, 1.0)  # HU range -2**10, 2**12-2**10

    def equalize(self, img, bins=None, fast_approx=False):
        self.read_params(img, bins)
        return self.apply(img, fast_approx=fast_approx)

    def read_params(self, img, bins=None):
        if bins is not None:
            self.bins = bins
        hist, _ = np.histogram(img, bins=self.bins, density=True)
        self.cdf = hist.cumsum()
        self.cdf /= self.cdf[-1]  # normalize to [0,1]

    def apply(self, img, fast_approx=False):
        if not fast_approx:
            img_eq = np.interp(img.ravel(), self.bins[:-1], self.cdf)
            img_eq = img_eq.reshape(img.shape)
        else:
            img_eq = self.cdf[(img + 1024).astype(int)]
        return img_eq

