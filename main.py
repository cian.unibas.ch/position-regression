import torch
import numpy as np
import setproctitle
import os.path
from pathlib import Path
from omegaconf import OmegaConf
from data.cq500dataset import SingleClassDataset
from torch.utils.data import DataLoader
from models.patchposreg import PatchPosRegModel
from models.autoencoder import AutoencoderModel
import nibabel as nib
from torch.utils.tensorboard import SummaryWriter

def train(cfg_cli):
    cfg_file = OmegaConf.load(cfg_cli.config_file or 'config.yaml')
    cfg = OmegaConf.merge(cfg_file, cfg_cli)
    device = torch.device(cfg.device)
    setproctitle.setproctitle(cfg.model.type)
    torch.manual_seed(cfg.seeds.torch)
    np.random.seed(cfg.seeds.numpy)

    writer = SummaryWriter()
    writer.add_text("config", OmegaConf.to_yaml(cfg))
    log_dir = writer.get_logdir()
    OmegaConf.save(cfg, os.path.join(log_dir, 'config.yaml'))

    if cfg.model.type == 'patchposreg':
        model = PatchPosRegModel(cfg, device=device, summary_writer=writer)
    elif cfg.model.type == 'autoencoder':
        model = AutoencoderModel(cfg, device=device, summary_writer=writer)
    else:
        raise NotImplementedError(f'{cfg.model.type=} is not implemented')
    model.net.print_number_of_parameters()

    torch.backends.cudnn.benchmark = cfg.training.cudnn_benchmark


    print('set up data')
    dataset = SingleClassDataset(cfg=cfg, part='train')
    dataloader = DataLoader(
        dataset=dataset,
        batch_size=cfg.training.batch_size,
        shuffle=True,
        num_workers=cfg.training.num_workers
    )
    print(cfg.training)

    print('loop over epochs')
    for epoch in range(cfg.training.num_epoch):
        # loop within epoch
        for idx, data in enumerate(dataloader):
            iteration_idx = epoch * len(dataloader) + idx
            # preprocess data
            img = data['img']
            img = img[:, None, ...].float().to(device)
            mask = data['mask']
            mask = mask[:, None, ...].to(device)

            model.optimize_step(img, mask=mask, epoch_idx=epoch, iteration_idx=iteration_idx)

            # generate intermediate output
            if (iteration_idx+1) % cfg.validation.validation_iter == 0:
                if not cfg.validation.skip:
                    model.generate_image(image=img, mask=mask, iteration_idx=iteration_idx)

        # save model
        model.save(os.path.join(log_dir, 'model_save.pt'), epoch_idx=epoch)
    pass # /train


def predict(cfg_cli):
    # set up config
    cfg = cfg_cli
    load_from_path = cfg.predict.load_from
    cfg_load_from = OmegaConf.load(os.path.join(load_from_path, 'config.yaml'))
    cfg = OmegaConf.merge(cfg, cfg_load_from, cfg_cli)
    # get device
    device = torch.device(cfg.device)
    # set title
    setproctitle.setproctitle(cfg.model.type)
    # set seeds
    torch.manual_seed(cfg.seeds.torch)
    np.random.seed(cfg.seeds.numpy)

    # set up model
    if cfg.model.type == 'patchposreg':
        model = PatchPosRegModel(cfg, device=device, summary_writer=None)
    elif cfg.model.type == 'autoencoder':
        model = AutoencoderModel(cfg, device=device, summary_writer=None)
    else:
        raise NotImplementedError(f'{cfg.model.type=} is not implemented')

    #load model
    model_save_fp = os.path.join(load_from_path, 'model_save.pt')
    model.load(model_save_fp)

    torch.backends.cudnn.benchmark = cfg.training.cudnn_benchmark

    dataset = SingleClassDataset(cfg=cfg, part='all')
    dataloader = DataLoader(
        dataset=dataset,
        batch_size=cfg.prediction.batch_size,
        shuffle=False,
        num_workers=cfg.prediction.num_workers
    )
    pred_path = os.path.join(load_from_path, 'predictions')
    Path(pred_path).mkdir(parents=True, exist_ok=True)
    for i, data in enumerate(dataloader):
        images = data['img'][None, ...].float().to(device)
        mask = data['mask'][None, ...].to(device)

        target_file = os.path.join(pred_path, data['name'][0])
        abnormal_score = model.predict(images, mask)

        final_nimg = nib.Nifti1Image(abnormal_score, affine=data['affine'][0])
        nib.save(final_nimg, target_file)
        print(f'saved to {target_file=}')


def main():
    cfg_cli = OmegaConf.from_cli()
    if cfg_cli.mode == 'train':
        train(cfg_cli)
    elif cfg_cli.mode == 'predict':
        predict(cfg_cli)
    else:
        raise NotImplementedError(f'mode = {cfg_cli.mode} is not implemented')

if __name__ == '__main__':
    main()