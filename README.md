# Position Regression for Unsupervised Anomaly Detection 
This is a PyTorch implementation of the paper *Position Regression for Unsupervised Anomaly Detection*, MIDL2022.


## Installation
We provide an `environment.yaml` file to create a conda environment.
If you have no conda installation, we recommend installing [miniconda](https://docs.conda.io/en/latest/miniconda.html).
You can create a new environment using `conda env create -f environment.yaml` which will create a new environment
called `posreg`. You can activate it using `conda activate posreg`.


## Training
To run the training of our model or the autoencoder baseline you can run

    main.py mode='train' model.type=patchposreg config_file=config_ppr.yaml data.image_path=/path/to/data/root
    main.py mode='train' model.type=autoencoder config_file=config_ae.yaml data.image_path=/path/to/data/root

You can specify the device for the computation by setting e.g. `device=cpu` or `device='cuda:0'` etc.

Each training run will create a summary directory in `runs/` which will contain the checkpoints, the configuration used
and the tensorboard logging file. You can view the latter by starting tensorboard with `tensorboard --logdir runs`,
and opening the indicated address in the browser.

## Prediction
After a successfull training a summary file as well as the complete configuration 
and model checkpoints is being written to `runs/[summary directory]`

    main.py mode='predict' predict.load_from='runs/[summary directory]'

## Data Layout
The data should be saved as a 256x256x256 volumes as nifti files along with their foreground/background masks.
The data directory should have two subdirectories  `dataset` defined as `data.image_path` containing the actual images
and `dataset_masks` containing files with the same names containing the foreground/background masks.
To use our dataset you must provide a `database_split.csv` with at least following three columns describing the dataset:

* `file` containing the relative path from `data.image_path` to the file.
* `train` containing a boolean indicating whether the image should be used during training
* `test` containing a boolean indicating whether the image should be used during testing/prediction

Alternatively you can just provide your own
[`torch.utils.data.Dataset`](https://pytorch.org/docs/stable/data.html#torch.utils.data.Dataset).
