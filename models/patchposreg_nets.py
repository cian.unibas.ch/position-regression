import torch
import torch.nn as nn
import numpy as np


class ResConvBlock(nn.Module):
    def __init__(self, inc, outc=None, downsample=True, dim=3):
        super().__init__()
        if outc is None:
            outc = inc
        if dim == 3:
            conv = nn.Conv3d
            instancenorm = nn.InstanceNorm3d
            pool = nn.AvgPool3d
        elif dim == 2:
            conv = nn.Conv2d
            instancenorm = nn.InstanceNorm2d
            pool = nn.AvgPool2d
        stride = 2 if downsample else 1
        self.residual = nn.Sequential(
            nn.utils.spectral_norm(conv(inc, outc, 3, stride, 1)),
            instancenorm(outc),
            nn.LeakyReLU(0.2)
        )
        if inc != outc:
            self.skip = nn.utils.spectral_norm(conv(inc, outc, 1, stride, 0, bias=False))
        else:
            self.skip = pool(1, stride, 0)

    def forward(self, x):
        res = self.residual(x)
        skip = self.skip(x)
        out = res + skip
        return out

class PatchPosReg(nn.Module):
    def __init__(self, cfg, dim=3):
        super().__init__()
        c = cfg.model.channels
        channels = [1*c, 2*c, 4*c, 8*c, 16*c]
        blocks = [ResConvBlock(inc=1, outc=channels[0], downsample=False, dim=dim)]
        for cin, cout in zip(channels, channels[1:]):
            blocks.extend([
                ResConvBlock(cin, cin, downsample=True, dim=dim),
                ResConvBlock(cin, cout, downsample=False, dim=dim)
            ])
        self.conv_blocks = nn.Sequential(*blocks)
        self.avg_pool = nn.AdaptiveAvgPool2d(1) if dim == 2 else nn.AdaptiveAvgPool3d(1)
        self.fc = nn.Sequential(
            nn.Linear(channels[-1], channels[-1]),
            nn.LeakyReLU(0.2),
            nn.Linear(channels[-1], dim),
            nn.Sigmoid()
        )

    def forward(self, x):
        conv = self.conv_blocks(x)
        lin = self.avg_pool(conv).view(conv.shape[0], -1)
        lin1 = self.fc(lin)
        return lin1*1.1 - 0.05

    def print_number_of_parameters(self):
        net = self
        name = self.__class__.__name__
        print("{} : {:_}".format(name, np.array([np.array(p.shape).prod()
            for p in net.parameters()]).sum()))
