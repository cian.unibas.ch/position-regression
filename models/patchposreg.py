import torch
import torch.nn as nn
import numpy as np
import models.utils as mutils
from models.patchposreg_nets import PatchPosReg
import diplib as dip

class PatchPosRegModel:
    def __init__(self, cfg, device=torch.device('cpu'), summary_writer=None):
        self.cfg = cfg
        self.device = device
        self.net = PatchPosReg(cfg, dim=3).to(device)
        self.optimizer = torch.optim.Adam(self.net.parameters(), lr=self.cfg.training.learning_rate)

        self.summary_writer = summary_writer

    def optimize_step(self, image, mask=None, epoch_idx=None, iteration_idx=None):
        self.epoch_idx = epoch_idx


        # make patches
        with torch.no_grad():
            patches, mask_patches, normalized_coords, coords = mutils.make_patch_batch(image, labels=mask,
                                                                         size=self.cfg.model.patch_size,
                                                                         patches_per_image=self.cfg.model.patches_per_image)  # create batch of patches
            normalized_coords = normalized_coords.to(self.device)
            nonempty_patches = (mask_patches.abs().sum(dim=tuple(range(2, len(patches.shape)))) > 0)
            ratio_of_nonempty_patches = nonempty_patches.float().mean()

            brain_only_patches = (~(mask_patches != 0)).sum(dim=tuple(range(2, len(patches.shape)))) == 0
            ratio_of_brain_only_patches = brain_only_patches.float().mean()

            halfempty_patches = nonempty_patches & ~brain_only_patches

        if ratio_of_brain_only_patches > ratio_of_nonempty_patches:
            print("more brain only than nonempty")
            breakpoint()

        # get predictions from model at patches
        pred = self.net(patches.reshape(-1, *patches.shape[2:]))
        pred = pred.reshape(*normalized_coords.shape)

        # set all previous gradients of the model to zero
        for param in self.net.parameters():
            param.grad = None

        # define the overall loss value
        loss_value = {}

        # weigh non-empty patches with 1, and empty patches with 0
        dists = ((pred - normalized_coords) ** 2).sum(dim=2).sqrt()
        loss_value['l_coords'] = (dists * nonempty_patches).sum() / (nonempty_patches.sum() + 1e-16)
        loss_value['r_nonempty'] = ratio_of_nonempty_patches
        loss_value['r_brainonly'] = ratio_of_brain_only_patches
        loss_value['l_brainonly'] = (dists * brain_only_patches).sum() / (brain_only_patches.sum() + 1e-16)
        loss_value['l_halfempty'] = (dists * halfempty_patches).sum() / (halfempty_patches.sum() + 1e-16)
        # evaluate the model for the current batch of images
        overall_loss_value = loss_value['l_coords']

        # save the overall train loss
        if self.summary_writer is not None and \
                iteration_idx is not None and \
                epoch_idx is not None:
            self.summary_writer.add_scalar('train_loss', overall_loss_value.item(), iteration_idx)
            for (name, loss) in loss_value.items():
                self.summary_writer.add_scalar(name, loss, iteration_idx)

            # generate the print out
            loss_string = ["Epoch", str(epoch_idx), "iter", str(iteration_idx), "loss",
                           str(np.round(overall_loss_value.item(), 5))]
            loss_string += ['{}: {:7.5f}\t'.format(a, b) for (a, b) in loss_value.items()]
            print(" ".join(loss_string))
        # compute the gradients of the model
        overall_loss_value.backward()

        # update the parameters of the model
        self.optimizer.step()


    def generate_image(self, image, mask=None, iteration_idx=0):
        with torch.no_grad():
            image = image * mask
            grid = mutils.eval_error_map_slices(self.net, image, self.cfg)
        self.summary_writer.add_image(f'out', grid, iteration_idx)

    def predict(self, images, mask=None):
        self.net.eval()
        error_map = mutils.generate_error_map_masked(
            self.net, images, mask,
            size=self.cfg.model.patch_size,
            max_batch_size=self.cfg.prediction.max_batch_size,
            device=self.cfg.device,
            subsampling_stride=self.cfg.prediction.subsampling_stride
        )
        self.net.train()

        abnormal_score = error_map.numpy()
        if self.cfg.prediction.erode:
            if erode := ((radius := self.cfg.prediction.erode_radius) > 1):
                se = dip.SE(shape='rectangular', param=(radius, radius, radius))
                img_dip = dip.Erosion(abnormal_score.astype(np.float32), se)
                abnormal_score = np.array(img_dip)
        if self.cfg.prediction.clip:
            abnormal_score -= self.cfg.prediction.clip_lower
            abnormal_score = abnormal_score.clip(min=0)
            abnormal_score /= (self.cfg.prediction.clip_upper - self.cfg.prediction.clip_lower)
            abnormal_score = abnormal_score.clip(max=1)

        return abnormal_score

    def save(self, fp, epoch_idx=None):
        if epoch_idx is None:
            epoch_idx = self.epoch_idx
        state = dict(
            model_state=self.net.state_dict(),
            optimizer_state=self.optimizer.state_dict(),
            epoch=self.epoch_idx
        )
        torch.save(state, fp)

    def load(self, fp):
        state = torch.load(fp, map_location=self.device)
        self.net.load_state_dict(state['model_state'])
        self.optimizer.load_state_dict(state['optimizer_state'])
        print(f'loaded model from epoch {state["epoch"]}')