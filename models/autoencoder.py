import torch
import torch.nn as nn
import numpy as np
import models.utils as mutils
from models.ae_nets import Autoencoder
import diplib as dip

class AutoencoderModel:
    def __init__(self, cfg, device=torch.device('cpu'), summary_writer=None):
        self.cfg = cfg
        self.device = device
        self.net = Autoencoder(cfg).to(device)
        self.optimizer = torch.optim.Adam(self.net.parameters(), lr=self.cfg.training.learning_rate)

        self.summary_writer = summary_writer

    def optimize_step(self, image, mask=None, epoch_idx=None, iteration_idx=None):
        self.epoch_idx = epoch_idx
        image = image * mask
        out = self.net(image)

        # set all previous gradients of the model to zero
        for param in self.net.parameters():
            param.grad = None

        # define the overall loss value
        loss_value = {}

        # compute loss
        # weigh non-empty patches with 1, and empty patches with 0
        diff = image - out
        dists = (diff.abs()).mean(dim=(1, 2, 3, 4)).mean()
        loss_value['l_autoenc'] = dists
        # evaluate the model for the current batch of images
        overall_loss_value = loss_value['l_autoenc']

        # save the overall train loss
        if self.summary_writer is not None and \
                iteration_idx is not None and \
                epoch_idx is not None:
            self.summary_writer.add_scalar('train_loss', overall_loss_value.item(), iteration_idx)
            for (name, loss) in loss_value.items():
                self.summary_writer.add_scalar(name, loss, iteration_idx)

            # generate the print out
            loss_string = ["Epoch", str(epoch_idx), "iter", str(iteration_idx), "loss",
                           str(np.round(overall_loss_value.item(), 5))]
            loss_string += ['{}: {:7.5f}\t'.format(a, b) for (a, b) in loss_value.items()]
            print(" ".join(loss_string))
        # compute the gradients of the model
        overall_loss_value.backward()

        # update the parameters of the model
        self.optimizer.step()


    def generate_image(self, image, mask=None, iteration_idx=0):
        with torch.no_grad():
            image = image * mask
            self.net.eval()
            out = self.net(image)
            self.net.train()
            diff = (image-out).abs()
            grid = torch.cat([
                torch.cat([
                    image[0, 0, 128, :, :],
                    image[0, 0, :, 128, :],
                    image[0, 0, :, :, 128],
                ], axis=0),
                torch.cat([
                    out[0, 0, 128, :, :],
                    out[0, 0, :, 128, :],
                    out[0, 0, :, :, 128],
                ], axis=0).clamp(min=0, max=1),
                torch.cat([
                    diff[0, 0, 128, :, :],
                    diff[0, 0, :, 128, :],
                    diff[0, 0, :, :, 128],
                ], axis=0).clamp(min=0, max=1),
            ], axis=1)
        self.summary_writer.add_image(f'out', grid, iteration_idx, dataformats='HW')

    def predict(self, images, mask=None):

        self.net.eval()
        with torch.no_grad():
            if mask is not None:
                images = images * mask
            out = self.net(images)
            error_map = (out - images).abs()
        self.net.train()

        abnormal_score = error_map[0,0,...].detach().cpu().numpy()
        if self.cfg.prediction.erode:
            if erode := ((radius := self.cfg.prediction.erode_radius) > 1):
                se = dip.SE(shape='rectangular', param=(radius, radius, radius))
                img_dip = dip.Erosion(abnormal_score.astype(np.float32), se)
                abnormal_score = np.array(img_dip)
        if self.cfg.prediction.clip:
            abnormal_score -= self.cfg.prediction.clip_lower
            abnormal_score = abnormal_score.clip(min=0)
            abnormal_score /= (self.cfg.prediction.clip_upper - self.cfg.prediction.clip_lower)
            abnormal_score = abnormal_score.clip(max=1)

        return abnormal_score


    def save(self, fp, epoch_idx=None):
        if epoch_idx is None:
            epoch_idx = self.epoch_idx
        state = dict(
            model_state=self.net.state_dict(),
            optimizer_state=self.optimizer.state_dict(),
            epoch=self.epoch_idx
        )
        torch.save(state, fp)

    def load(self, fp):
        state = torch.load(fp, map_location=self.device)
        self.net.load_state_dict(state['model_state'])
        self.optimizer.load_state_dict(state['optimizer_state'])
        print(f'loaded model from epoch {state["epoch"]}')