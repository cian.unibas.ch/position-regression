import torch
import torch.nn.functional as tf
import numpy as np
import time
import tqdm
import diplib as dip


def count_parameters(net):
    if isinstance(net, torch.nn.Module):
        print("{:_}".format(np.array([np.array(p.shape).prod()
            for p in net.parameters()]).sum()))
    else:
        return 0


def make_patch_batch(batch, labels=None, size=32, patches_per_image=None):
    """
    takes batch of shape (B, C, X, Y[, Z])
    samples patches (B, patches_per_image, C, size, size[, size])
    and returns their coordinates as integer coords and as normalized coords (to [0, 1]^k)
    if patches_per_image is None, all possible patches will be computed
    """
    bs, channels, *dims = batch.shape
    dim = len(dims)
    labels_present = labels is not None
    for k in range(dim):
        batch = batch.unfold(2+k, size, 1)
        if labels_present:
            labels = labels.unfold(2+k, size, 1)
    # sample patches
    windows = batch.shape[2:2+dim]  # number of sliding windows in each direction
    if patches_per_image is None:  # return all of them
        coords = torch.meshgrid(*[torch.arange(k) for k in windows])
        coords = torch.stack(coords, dim=-1)
        coords = coords.reshape(1, -1, coords.shape[-1])
        coords = coords.expand(bs, *coords.shape[1:])
        newshape = (*batch.shape[:2], -1, *batch.shape[-dim:])
        patches = batch.reshape(newshape)
        if labels_present:
            label_patches = labels.reshape(newshape)
        patches = patches.transpose(1, 2)  # swap patches_per_image and channel
    else:  # randomly sample
        coords = np.random.randint(0, windows, (bs, patches_per_image, dim))
        bind, *_ = np.ogrid[:bs, :patches_per_image]
        ind = (bind, slice(None), *coords[:, :, :].transpose(2, 0, 1), ...)
        patches = batch[ind]
        if labels_present:
            label_patches = labels[ind]
    normalized_coords = coords / (torch.tensor(windows) - 1)
    if not labels_present:
        label_patches = None
    return patches, label_patches,  normalized_coords, coords


def make_patch_grid(batch, labels=None, size=32, patches_by_dim=(120, None, None)):
    """
    takes batch of shape (B, C, X, Y[, Z])
    extracts batches from indices given by patches_by_dim
    and returns their coordinates as integer coords and as normalized coords (to [0, 1]^k)
    if patches_per_image is None, all possible patches will be computed
    """
    bs, channels, *dims = batch.shape
    dim = len(dims)
    assert len(patches_by_dim) == dim, "length of patches_by_dim does not match number of dimensions"
    # make patches
    for k in range(dim):
        batch = batch.unfold(2+k, size, 1)
        if labels is not None:
            labels = labels.unfold(2+k, size, 1)
    windows = batch.shape[2:2+dim]  # number of sliding windows in each direction
    patches_by_dim = list(patches_by_dim)
    for i, p in enumerate(patches_by_dim):
        if type(p) == int:
            p = torch.tensor([p])
        elif type(p) == list:
            p = torch.tensor(p)
        elif type(p) in [slice, range]:
            start = max(0, p.start) if p.start is not None else 0
            end = min(windows[i], p.stop) if p.stop is not None else windows[i]
            step = p.step if p.step is not None else 1
            p = torch.arange(start=start, end=end, step=step)
        elif p is None:
            p = torch.arange(0, windows[i])
        patches_by_dim[i] = p
    # sample patches
    coords = torch.meshgrid(*patches_by_dim)
    coords = torch.stack(coords, dim=-1)
    coords = coords[None, ...].expand(bs, *coords.shape).reshape(bs, -1, dim)
    patches_per_image = coords.shape[1]
    bind, *_ = np.ogrid[:bs, :patches_per_image]
    ind = (bind, slice(None), *coords[:, :, :].permute([2, 0, 1]), ...)
    patches = batch[ind]
    if labels is not None:
        label_patches = labels[ind]
    else:
        label_patches = None
    normalized_coords = coords / (torch.tensor(windows) - 1)
    return patches, label_patches, normalized_coords, coords

def generate_error_map_slice(model, images, masks=None, size=32, max_batch_size=None, fixed_dim=0, slice_idx=120, half_samples=False):
    """
    generate a reconstruction error map for 3d volumes. This will compute
    the error map along the slice `slice_idx` along dimension `fixed_dim`
    `max_batch_size` sets the maximal number of patches processed simultaneously
    """
    patches_by_dim = [None, None, None]
    if half_samples:
        patches_by_dim = [slice(0, None, 12) for _ in patches_by_dim]
    patches_by_dim[fixed_dim] = slice_idx
    patches, mask_patches, normalized_coords, coords = make_patch_grid(
        images,
        labels=masks,
        size=size,
        patches_by_dim=patches_by_dim)
    normalized_coords = normalized_coords.to(images.device)
    model.eval()
    patches_batched = patches.reshape(-1, *patches.shape[2:])
    with torch.no_grad():
        pred = []
        total_patches = patches_batched.shape[0]
        for k in tqdm.tqdm(range(0, total_patches, max_batch_size)):
            batch_idx = list(range(k, min(k+max_batch_size, total_patches)))
            pred.append(model(patches_batched[batch_idx, ...]))
        pred = torch.cat(pred, dim=0).reshape(*normalized_coords.shape)
        dists = ((pred - normalized_coords)**2).sum(dim=2).sqrt()
        error_map_shape = [images.shape[k] for k in [2, 3, 4]]
        error_map_shape[fixed_dim] = 1
        error_map = torch.zeros(error_map_shape)
        # we need the (+ size//2) to account for patch size
        indices = tuple(coords[..., k].cpu() + size//2 for k in range(coords.shape[-1]))
        indices[fixed_dim][...] = 0
        error_map.index_put_(indices, dists.cpu())
    model.train()
    return error_map


def generate_error_map_full(model, images, size=32, max_batch_size=None):
    """
    generate a reconstruction error map for 3d volumes.
    `max_batch_size` sets the maximal number of patches processed simultaneously
    """
    t = time.time()
    with torch.no_grad():
        error_map_shape = images.shape[2:5]
        error_map = torch.zeros(error_map_shape)
        model.eval()
        for slice_idx in tqdm.tqdm(range(images.shape[-1]-size+1)):
            patches_by_dim = [None, None, slice_idx]
            patches, normalized_coords, coords = make_patch_grid(images,
                                                                 size=size,
                                                                 patches_by_dim=patches_by_dim)
            normalized_coords = normalized_coords.to(images.device)
            patches_batched = patches.reshape(-1, *patches.shape[2:])
            pred = []
            total_patches = patches_batched.shape[0]
            for k in range(0, total_patches, max_batch_size):
                batch_idx = list(range(k, min(k+max_batch_size, total_patches)))
                pred.append(model(patches_batched[batch_idx, ...]))
            pred = torch.cat(pred, dim=0).reshape(*normalized_coords.shape)
            dists = ((pred - normalized_coords)**2).sum(dim=2).sqrt()
            # we need the (+ size//2) to account for patch size
            indices = tuple(coords[..., k].cpu() + size//2 for k in range(coords.shape[-1]))
            error_map.index_put_(indices, dists.cpu())
    model.train()
    return error_map


def generate_error_map_masked(model, images, mask, size=32,
                              max_batch_size=None, device=None, subsampling_stride=1):
    """
    generate a reconstruction error map for 3d volumes.
    `max_batch_size` sets the maximal number of patches processed simultaneously
    """
    t = time.time()
    if device is None:
        device = images.device
    if type(subsampling_stride) is int:
        subsampling_stride = 3*(subsampling_stride,)
    with torch.no_grad():
        error_map_shape = images.shape[2:5]
        error_map = torch.zeros(error_map_shape, device=device)
        model.eval()
        # Preprocessing: fill holes and close
        if mask is None:
            mask = fill_and_close(images)
        else:
            mask = mask.squeeze()

        for slice_idx in tqdm.tqdm(range(0, images.shape[-1]-size+1,  subsampling_stride[2])):
            patches_by_dim = [slice(None, None, subsampling_stride[0]),
                    slice(None, None, subsampling_stride[1]),
                    slice_idx]
            patches, label_patches, normalized_coords, coords = make_patch_grid(images,
                                                                 size=size,
                                                                 patches_by_dim=patches_by_dim)
            normalized_coords = normalized_coords.to(device)
            patches_batched = patches.reshape(-1, *patches.shape[2:]).to(device)
            pred = []
            total_patches = patches_batched.shape[0]
            #filter out unmasked
            coords_mask = mask[tuple(coords[0,:,k]+size//2 for k in range(coords.shape[-1]))]
            masked_coords = coords[:, coords_mask, :]
            masked_total_patches = masked_coords.shape[1]
            if masked_total_patches == 0:
                continue
            batch_indices = torch.arange(total_patches)[coords_mask]
            for k in range(0, masked_total_patches, max_batch_size):
                batch_idx = batch_indices[k:min(k+max_batch_size, total_patches)]
                batch = patches_batched[batch_idx, ...]
                pred.append(model(batch))
            pred = torch.cat(pred, dim=0).reshape(*masked_coords.shape)
            masked_normalized_coords = normalized_coords[:, coords_mask, :]
            dists = ((pred - masked_normalized_coords)**2).sum(dim=2).sqrt()
            # we need the (+ size//2) to account for patch size
            indices = tuple(masked_coords[..., k].cpu() + size//2 
                            for k in range(masked_coords.shape[-1]))
            error_map.index_put_(indices, dists)#.cpu())
            del patches_batched
        # after processing with subsampling_stride > 0 we need to interpolate
        error_map_dip = dip.Image(error_map.cpu().numpy(), None)
        box_size = tuple(reversed(subsampling_stride))
        se = dip.SE(shape='rectangular', param=box_size)
        error_map_dip = dip.Dilation(error_map_dip, se, ['add min'])
        error_map = torch.tensor(np.array(error_map_dip))
        error_map[~mask] = 0
                
    model.train()
    return error_map


def fill_and_close(img):
    t = time.time()
    img = img[0, 0, ...]
    nonzero = (img > 0).cpu().numpy()
    filled = dip.FillHoles(nonzero)
    se = dip.SE(shape="rectangular", param=(3,3,3))
    closed = dip.Closing(filled, se, ["add min"])
    print(f"fill & close: {time.time()-t}")
    closed = torch.tensor(np.array(closed))
    return closed 


def pad_to_like_2d(inp, like, debug=False): 
    """
    input is assumed to be [1, H, W]
    """
    inp_shape = torch.tensor(inp.shape)
    like_shape = torch.tensor(like.shape)
    halfs = (like_shape - inp_shape)/2
    pads = [halfs[1].ceil(), halfs[1].floor(),
            halfs[2].ceil(), halfs[2].floor()]
    pads = tuple(int(p.item()) for p in pads)
    out = tf.pad(inp, pads)
    if debug:
        print("debugging in pad to like 2d")
        breakpoint()
    return out


def eval_error_map_slices(model, image, cfg, max_batch_size=None):
    """
    evaluate error map on three cross sections
    """
    if max_batch_size is None:
        max_batch_size = cfg.model.patches_per_image
    error_map_grid = []
    error_map_nobg_grid = []
    img_grid = []
    for d in range(3):  # add error map slice in all three dimensions
        img_slice = image[(0, 0, *(slice(None) if k != d else 120 for k in range(3)))].squeeze().unsqueeze(0).cpu()
        error_map = generate_error_map_slice(model=model,
                                             images=image[0:1, ...],
                                             max_batch_size=max_batch_size,
                                             size=cfg.model.patch_size,
                                             fixed_dim=d,
                                             half_samples=False)
        error_map = error_map.squeeze().unsqueeze(0)
        error_map_grid.append(error_map.cpu())
        # remove background
        error_map = error_map.clone()
        error_map[img_slice == 0] = 0
        error_map_nobg_grid.append(error_map.cpu())
        img_grid.append(img_slice)
    grid = torch.cat([torch.cat(img_grid, dim=1),
                      torch.cat(error_map_nobg_grid, dim=1),
                      torch.cat(error_map_grid, dim=1)
                      ], dim=2)
    return grid
