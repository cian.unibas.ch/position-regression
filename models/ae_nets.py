import torch
import torch.nn as nn
import numpy as np


class Autoencoder(nn.Module):
    def __init__(self, cfg=dict()):
        super().__init__()
        c = cfg.model.channels
        self.channels = [1, 1*c, 2*c, 4*c, 8*c, 16*c]
        self.make_encoder()
        self.make_decoder()

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        x = x.sigmoid() * 1.1 - 0.05
        return x

    def make_encoder(self):
        channels = self.channels
        l = []
        for cin, cout in zip(channels[:-2], channels[1:]):
            l.append(nn.Conv3d(in_channels=cin, out_channels=cout, kernel_size=4, stride=2, padding=1))
            l.append(nn.BatchNorm3d(num_features=cout))
            l.append(nn.LeakyReLU())
        l.append(nn.Conv3d(in_channels=channels[-2], out_channels=channels[-1], kernel_size=1, stride=1, padding=0))
        l.append(nn.LeakyReLU())
        self.encoder = nn.Sequential(*l)

    def make_decoder(self):
        channels = self.channels[::-1]
        l = []
        l.append(nn.Conv3d(in_channels=channels[0], out_channels=channels[1], kernel_size=1, stride=1, padding=0))
        l.append(nn.LeakyReLU())
        for cin, cout in zip(channels[1:], channels[2:]):
            #l.append(nn.ReflectionPad3d(padding=(0, 0)*3))
            l.append(nn.ConvTranspose3d(in_channels=cin, out_channels=cout, kernel_size=4, stride=2, padding=1))
            l.append(nn.BatchNorm3d(num_features=cout))
            l.append(nn.LeakyReLU())
        l.pop()
        l.pop()
        self.decoder = nn.Sequential(*l)

    def print_number_of_parameters(self):
        net = self
        name = self.__class__.__name__
        print("{} : {:_}".format(name, np.array([np.array(p.shape).prod()
                                                 for p in net.parameters()]).sum()))

